﻿/*
* @Author - José Humberto Ibarra Riestra //Javier Armenta
*/
$(function () {
    /*
    * USER OBJECT
    */
    window.ROOT.Home = function () {

        var utils = ROOT.Utils,
           txtDesc = $("#txtDesc"),
           txtname = $("#txtName"),
            select2 = $(".select2"),
            hidCsv = $(".hidCsv"),
            txtXml = $("#txtSql"),
            txtid = $("#txtid"),
            txtSql = $("#txtSql"),
            btnAdd = $("#btnAdd"),
            rows = "#tblPackages tbody tr",
            btnSave = $("#btnSave"),
            btnUpdate = $("#btnUpdate"),
            btnClear = $("#btnClear"),
             tblUsers = $("#tblUsers tbody"),
            editor = null,
            xmlData = null


        function initTable() {
            tableResults = utils.createScrollableTable({
                table: tableResults,
                controller: "Results",
                action: "FindByFilters",
                updateParams: function () {
                    return {
                        idName: txtName.val()
                    };
                }
            });
            tableResults.update();
        }

        function init() {
 
           // edit(1);
            initSelect2();
            initEvents();
            editor = CodeMirror.fromTextArea(txtXml[0], {
                // your settings here
                mode: 'text/x-mariadb',
          
                lineNumbers: true
            });
            editor.setSize(900, 900);
           /// editor.setValue("select * from dual");
       
        }
        function initSelect2() {
            select2.select2({
                width: "100%",
                placeholder: "-Please Select-"
            });
        }


        function initEvents() {
            hidCsv.on("change", function (e) {
                e.preventDefault();
                var file = hidCsv.prop("files")[0];
                if (file.name.indexOf(".xml") == -1) {
                    swal("File not Valid", "Only XML files are allowed", "error");
                    return false;
                }
                readFile(file);
            });
            btnAdd.click(function (e) {
                if (validateForm()) {
                    createRow(tblPackages, getFormInfo());
                    txtDocTypeId.val("");
                    txtDoctypeDesc.val("");
                    txtDocTypeId.focus();
                }
            });
            btnSave.click(function (e) {
                if (validateForm()) {
                    save();
                }
            });
            btnUpdate.click(function (e) {
                if (validateForm()) {
                    //  save();
                    update();
                }
            });
            //btnUpdate.click(function (e) {
            //    if (validateForm()) {
            //        save();
            //    }
            //});
            
            btnClear.click(function (e) {  clear();   });
        }

         
        function edit(id) {
            if (id) {
                utils.asyncRequest("GET", "Query", "FindById", { id: id }, false, null, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                    //    createEditModal(response.Data);
                    } else if (response.Status == utils.responseStatus.business) {
                       // utils.createModalInfo(response.Message);
                    }
                });
            }
        }

   


        //SAVE  QUERY
        function save() {
            var query = getFormInfo();
            utils.asyncRequest("POST", "Query", "Save", query, true, "Saving Query...", function (response) {
                if (response.Status == utils.responseStatus.ok) {
                    window.setTimeout(function () {
                        clear();
                        swal("Success", response.Message, "success");
                    });
                } else if (response.Status == utils.responseStatus.business) {
                    swal("Error", response.Message, "error");
                }
            });

        }

        function update() {
            var query = getFormInfou();
            utils.asyncRequest("POST", "Query", "Save", query, true, "Saving Query...", function (response) {
                if (response.Status == utils.responseStatus.ok) {
                    window.setTimeout(function () {
                        clear();
                        swal("Success", response.Message, "success");
                    });
                } else if (response.Status == utils.responseStatus.business) {
                    swal("Error", response.Message, "error");
                }
            });

        }

        function readFile(file) {
            var reader = new FileReader();
            reader.onload = readSuccess;
            function readSuccess(event) {
                var csvResult = event.target.result;
                //txtXml.val(csvResult);
                editor.getDoc().setValue(csvResult);
                xmlData = csvResult;
            }
            reader.readAsText(file, 'ISO-8859-1');
        }

        function createRow(table, formInfo) {
            var tr = $("<tr>");
            var openCellTag = '<td>';
            var closeCellTag = '</td>';
            var btnRemove = '<td><button class="btn btn-danger" type="button" name="btnRemove" id="btnRemove"><span class="glyphicon glyphicon-remove"></span></button></td>';
            remove = $(btnRemove);
            remove.click(function (e) {
                $(this).closest('tr').remove();
            });
            tr.append($(openCellTag + formInfo.DoctypeId + closeCellTag + openCellTag + formInfo.DoctypeDescription + closeCellTag));
            tr.append(remove);
            table.find("tbody").append(tr);
        }

        function getFormInfo() {
            return {
                
                query_name: txtname.val(),
                description: txtDesc.val(),
                query_text: editor.getValue()
            };
        }


        function getFormInfou() {
            return {
                id: txtid.val(),
                query_name: txtname.val(),
                description: txtDesc.val(),
                query_text: editor.getValue()
            };
        }

     

        function validateForm() {
            if (txtDesc.val() == "" || txtDesc.val() == null || txtDesc.val() == undefined) {
                swal("Error", "Description is required...", "error");
                return false;
            }

            if (txtname.val() == "" || txtname.val() == null || txtname.val() == undefined) {
                swal("Error", "Name is required...", "error");
                return false;
            } else {
                return true;
            }
            if (txtSql.val() == "" || txtSql.val() == null || txtSql.val() == undefined) {
                swal("Error", "Sql Query is required...", "error");
                return false;
            } else {
                return true;
            }
        }


        function getTableInformation() {
            var data = [];
            var tableRows = $(rows);
            $(tableRows).each(function () {
                var $this = $(this);
                var row = {};
                var doctypeId = $this.find("td:first-child").text().trim();
                var docTypeDescription = $this.find("td:nth-child(2)").text().trim();
                row.DoctypeId = doctypeId;
                row.DocTypeDescription = docTypeDescription;
                data.push(row);
            });
            return data;
        }

         


        function clear() {
            editor.setValue("");
            txtDesc.val("");
            txtname.val("");
        }

        init();
         
        return {
            edit: edit
        };
        //

    }();
});
