﻿/*
* @Author - Javier Armenta Campos
*/
$(function () {

    window.ROOT.User = function () {

        var utils = ROOT.Utils,
            adminMenu = $("#adminMenu"),
            searchForm = $("#searchForm"),
            txtSearchUser = $("#txtSearchUser"),
            btnSearch = $("#btnSearch"),
            btnClearSearch = $("#btnClearSearch"),
            btnAddUser = $("#btnAddUser"),
            tblUsers = $("#tblUsers tbody"),
            tableUsers,
            dialogo = "",
            id = 0

        function init() {
            adminMenu.addClass("active");
            initEvents();
            initTable();
            txtSearchUser.focus();
            utils.checkIfSessionExist();
        }

        function initEvents() {
            searchForm.submit(function (e) {
                search();
                txtSearchUser.val("");
                txtSearchUser.focus();
                e.preventDefault();
            });
            btnSearch.click(function (e) { search(); });
            btnClearSearch.click(function (e) { clearSearch(); });
            btnAddUser.click(function (e) {
                utils.redirect("Users", "AddUser");
            });
        }

        function initTable() {
            tableUsers = utils.createScrollableTable({
                table: tblUsers,
                controller: "Users",
                action: "FindByFilters",
                updateParams: function () {
                    return {
                        idName: txtSearchUser.val()
                    };
                }
            });
            tableUsers.update();
        }

        function search() {
            tableUsers.clearAndUpdate();
        }

        function createEditModal() {
            utils.createEditModal();
        }

        function clearSearch() {
            tableUsers.clearAndUpdate();
            txtSearchUser.val("");
        }

        function edit(id) {
            if (id) {
                utils.asyncRequest("GET", "Users", "FindById", { id: id }, false, null, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        createEditModal(response.Data);
                    } else if (response.Status == utils.responseStatus.business) {
                        utils.createModalInfo(response.Message);
                    }
                });
            }
        }

        function createEditModal(data) {
            var btnOk = '<button type="button" class="btn btn-info" autofocus><span class="glyphicon glyphicon-ok"></span>OK</button>';
            var modal = "";
            modal += '<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
            modal += '<div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close"data-dismiss="modal">';
            modal += '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel"><strong>Edit Operator</strong></h4></div>';
            modal += '<div class="modal-body"><form class="form-horizontal" role="form"><div class="form-group"><label  class="col-sm-2 control-label"for="txtUserName">Op ID</label>';
            modal += '<div class="col-sm-10"><input type="text" class="form-control" id="txtUserName" name="txtUserName" placeholder="User Name" maxlength="50" /></div></div>';
            modal += '<div class="form-group"><label class="col-sm-2 control-label"for="txtFirstName" >First Name</label><div class="col-sm-10">';
            modal += '<input type="text" class="form-control" id="txtFirstName" name="txtFirstName" placeholder="First Name" maxlength="100" /></div></div>';
            modal += '<div class="form-group"><label class="col-sm-2 control-label"for="txtLastName" >Last Name</label><div class="col-sm-10">';
            modal += '<input type="text" class="form-control" id="txtLastName" name="txtLastName" placeholder="Last Name" maxlength="100" /></div></div>';
            modal += '<div class="form-group"><label class="col-sm-2 control-label"for="txtPassword" >Password</label><div class="col-sm-10">';
            modal += '<input type="password" class="form-control" id="txtPassword" name="txtPassword" placeholder="Password" maxlength="20" /></div></div>';
            /*modal += '<div class="form-group"><label class="col-sm-2 control-label"for="txtSite" >Site Name</label><div class="col-sm-10">';
            modal += '<input type="text" class="form-control" id="txtSite" name="txtSite" placeholder="Site Name" maxlength="50" />';*/
            modal += '<div class="form-group"><div class="col-sm-offset-2 col-sm-10"><div class="checkbox"><label> <input id="chkAvailable"type="checkbox"/>Available';
            modal += '</label></div></div></div>';
            modal += '<div class="form-group"><div class="col-sm-offset-2 col-sm-10"><div class="checkbox"><label> <input id="chkAdmin"type="checkbox"/>Admin';
            modal += '</label></div></div></div>';
            modal += '<div class="form-group"><div class="col-sm-offset-2 col-sm-10"><div class="checkbox"><label id="chk"><input id="checkOpen"type="checkbox"/>Open </label></div></div></div></form></div>';
            modal += '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" class="btn btn-info">Update';
            modal += '</button></div></div></div></div>';
            dialogo = $(modal);
            var btnOk = dialogo.find(".btn-info");
            var txtUserName = dialogo.find("#txtUserName");
            var txtFirstName = dialogo.find("#txtFirstName");
            var txtLastName = dialogo.find("#txtLastName");
            var txtPassword = dialogo.find("#txtPassword");
            var txtSite = dialogo.find("#txtSite");
            var chkbox = dialogo.find("#chkAvailable");
            var chkAdmin = dialogo.find("#chkAdmin");
            var chk = dialogo.find("#chk");
            var checkOpen = dialogo.find("#checkOpen");
            chk.hide();

            id = data.Id;
            txtUserName.val(data.UserName);
            txtFirstName.val(data.FirstName);
            txtLastName.val(data.LastName);
            txtPassword.val(data.Password);
            txtSite.val(data.SiteName);
            chkbox.prop("checked", data.Available);
            chkAdmin.prop("checked", data.UserType);
            btnOk.click(function (e) {
                var user = getFormInfo(id, txtUserName, txtFirstName, txtLastName, txtPassword, txtSite, chkbox, chkAdmin);
                if (user) {
                    save(user);
                }
            });
            dialogo.modal("show");
        }

        function getFormInfo(id, txtUserName, txtFirstName, txtLastName, txtPassword, txtSite, checkAvailable, chkAdmin) {
            return {
                op_id: id,
                op_user: txtUserName.val(),
                op_first_name: txtFirstName.val(),
                op_last_name: txtLastName.val(),
                op_password: txtPassword.val(),
                site_name: txtSite.val(),
                available: checkAvailable.prop("checked"),
                user_type: chkAdmin.prop("checked")
            };
        }

        function save(user) {
            utils.asyncRequest("POST", "Users", "Save", user, false, null, function (response) {
                if (response.Status == utils.responseStatus.ok) {
                    txtSearchUser.focus();
                    search();
                    dialogo.modal("hide");
                    swal("Info.", response.Message, "info");
                } else if (response.Status == utils.responseStatus.business) {
                    swal("Error.", response.Message, "error");
                }
            });
        }

        function remove(id) {
            utils.createModalConfirm("Are you sure you want to delete the selected Query?", function () {
                utils.asyncRequest("POST", "Users", "Delete", { id: id }, false, null, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        search();
                        swal("Info.", response.Message, "info");
                    } else if (response.Status == utils.responseStatus.business) {
                        swal("Error.", response.Message, "error");
                    }
                });
            });
        }

        init();
        return {
            edit: edit,
            remove: remove
        };
    }();
});
