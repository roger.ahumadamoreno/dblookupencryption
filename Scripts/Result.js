﻿/*
* @Author - Javier Armenta Campos
*/
$(function () {

    window.ROOT.Result = function () {

        var utils = ROOT.Utils,
            tblResults = $("#tblResults"),
            txtSearchQuery = $("#txtSearchQuery"),
            btnSearchIcon = $("#btnSearchIcon"),
            btnClearSearch = $("#btnClearSearch"),
            btnSearch = $("#btnSearch"),
            tableResults,
            id = 0

        function init() {
            initTable();
            initEvents();
        }
        // Sorting and filter
        $(function () {
            $("#tblResults").tablesorter({
                /*textExtraction: 
                {
                    1: function(node, table, cellIndex){ 
                        return $(node).find("input").val(); 
                    },
                    2: function(node, table, cellIndex){ 
                        return $(node).find("input").val(); 
                    },
                    3: function(node, table, cellIndex){ 
                        return $(node).find("select :selected").text();
                    }
                },*/
                // initialize zebra striping and filter widgets
                theme: "bootstrap",
                widgets: ["filter"],
                ignoreCase: true,
                widgetOptions: {
                    filter_resetOnEsc: true,
                }
            });
        });



        function initEvents() {
            btnSearch.click(function (e) {
                search();
                e.preventDefault();
            });
            btnSearchIcon.click(function (e) {
                search();
                e.preventDefault();
            });
        }

        function initTable() {
            tableResults = utils.createScrollableTable({
                table: tblResults,
                controller: "Results",
                action: "FindByFilters",
                updateParams: function () {
                    return {
                        idName: txtSearchQuery.val()
                    };
                }
            });
            tableResults.update();
        }

        function search() {
            tableResults.clearAndUpdate();
        }
      
        //

        //////tigere

        //function update() {
        //    var query = getFormInfou();
        //    utils.asyncRequest("POST", "Query", "Save", query, true, "Saving Query...", function (response) {
        //        if (response.Status == utils.responseStatus.ok) {
        //            window.setTimeout(function () {
        //                clear();
        //                swal("Success", response.Message, "success");
        //            });
        //        } else if (response.Status == utils.responseStatus.business) {
        //            swal("Error", response.Message, "error");
        //        }
        //    });

        //}
        //function getFormInfou(id) {
        //    return {
        //        id: id,
        //        query_name: txtname.val(),
        //        description: txtDesc.val(),
        //        query_text: editor.getValue()
        //    };
        //}
        /////

      
        function edit(id) {
            if (id) {
                utils.asyncRequest("GET", "Results", "FindById", { id: id }, false, null, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        createEditModal(response.Data);
                    } else if (response.Status == utils.responseStatus.business) {
                        utils.createModalInfo(response.Message);
                    }
                });
            }
        }

        function createEditModal(data) {
            var btnOk = '<button type="button" class="btn btn-info" autofocus><span class="glyphicon glyphicon-ok"></span>OK</button>';
            var modal = "";
            modal += '<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
            modal += '<div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close"data-dismiss="modal">';
            modal += '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel"><strong>Edit Query</strong></h4></div>';
            modal += '<div class="modal-body"><form class="form-horizontal" role="form"><div class="form-group"><label  class="col-sm-2 control-label"for="txtDescription">Description</label>';
            modal += '<div class="col-sm-10"><input type="text" class="form-control" id="txtDescription" name="txtDescription" placeholder="Description" maxlength="50" /></div></div>';
            modal += '<div class="form-group"><label class="col-sm-2 control-label"for="txtQueryName" >Query Name</label><div class="col-sm-10">';
            modal += '<input type="text" class="form-control" id="txtQueryName" name="txtQueryName" placeholder="Query Name" maxlength="100" /></div></div>';
            modal += '<div class="form-group"><label class="col-sm-2 control-label"for="txtQueryText" >Query</label><div class="col-sm-10">';
            modal += '<textarea rows="20" class="form-control codemirror-textarea" id="txtQueryText" name="txtQueryText"  placeholder="Query" spellcheck="false" ></textarea></div></div>';
            // modal += '<textarea rows="30" class="form-control codemirror-textarea" id="txtQueryText" name="txtQueryText"  placeholder="Query" spellcheck="false" ></textarea></div></div>';
            modal += '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button><button type="button" class="btn btn-info">Update';
            modal += '</button></div></div></div></div>';
            dialogo = $(modal);
            var btnOk = dialogo.find(".btn-info");
            var txtDescription = dialogo.find("#txtDescription");
            var txtQueryName = dialogo.find("#txtQueryName");
            var txtQueryText = dialogo.find("#txtQueryText");
  
             

            id = data.id;
            txtDescription.val(data.description);
            txtQueryName.val(data.query_name);
            txtQueryText.val(data.query_text);
            
            btnOk.click(function (e) {
                var Results = getFormInfo(id, txtDescription, txtQueryName, txtQueryText);
                if (Results) {
                    save(Results);
                }
            });
            dialogo.modal("show");
        }
        function getFormInfo(id, txtDescription, txtQueryName, txtQueryText) {
            return {
                id: id,
                description: txtDescription.val(),
                query_name: txtQueryName.val(),
                query_text: txtQueryText.val()

                
            };
        }

        function save(Results) {
            utils.asyncRequest("POST", "Results", "Save", Results, false, null, function (response) {
                if (response.Status == utils.responseStatus.ok) {
                    txtSearchQuery.focus();
                    search();
                    dialogo.modal("hide");
                    swal("Info.", response.Message, "info");
                } else if (response.Status == utils.responseStatus.business) {
                    swal("Error.", response.Message, "error");
                }
            });
        }

////
    
        init();
        ///
        return {
            edit: edit  
        };
        //

    }();
});
