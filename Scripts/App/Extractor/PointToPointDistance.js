﻿/*
* @Author - Rosario Esquer
*/
$(function () {

    window.ROOT.PointToPointDistance = function () {

        var utils = ROOT.Utils,
            adminMenu = $("#adminMenu"),
            btnCalculate = $("#btnCalculate"),
            txtFirstName = $("#txtFirstName"),
            txtLastName = $("#txtLastName"),
            txtAccount = $("#txtAccount"),
            txtPassword = $("#txtPassword"),
            frmDistances = $("#frmDistances"),
            btnClear = $("#btnClear"),
            txtSite = $("#txtSite"),
            chkAdmin = $("#chkAdmin")
            txt_origin = $("#txt_origin")
            txt_destination = $("#txt_destination")
            id = 0

        function init() {
            adminMenu.addClass("active");
            initValidations();
            initEvents();
        }

        function initValidations() {
            /* frmUser.validate({
                 rules: {
                     txtFirstName: { required: true },
                     txtLastName: { required: true },
                     txtAccount: { required: true },
                     txtPassword: { required: true }
                 }
             });*/
        }

        function initEvents() {
            btnCalculate.click(function (e) { save(); });
            btnClear.click(function (e) { clearForm(); });
        }

        function getFormInfo() {
            return {
                op_id: id,
                op_first_name: txtFirstName.val(),
                op_last_name: txtLastName.val(),
                op_user: txtAccount.val(),
                op_password: txtPassword.val(),
                site_name: txtSite.val(),
                available: chkAvailable.prop("checked"),
                user_type: chkAdmin.prop("checked")
            };
        }

        function clearForm() {
            id = 0;
            txt_origin.val("");
            txt_destination.val("");
            utils.resetForm(frmDistances);
        }

        function validForm() {
            var validations = "";
            if (txt_origin.val() == "") { validations = "Origin"; }
            if (txt_destination.val() == "") { validations = validations + "\n Destination "; }
            //alert("alerta!!");
            swal("Here's a message! Eroorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lorem erat, tincidunt vitae ipsum et, pellentesque maximus enim. Mauris eleifend ex semper, lobortis purus sed, pharetra felis", "error")
            //swal("Error. Error. Error. Error. Error. Error.", "These fields are required: \n" + validations + "These fields are required These fields are required", "error");
            if (validations != "") {
                swal("Error.", "These fields are required: \n" + validations, "error");
                return false;
            }
            else {
                return true;
            }

        }

        function save() {
            //if (frmUser.valid()) {
            if (validForm()) {
                var user = getFormInfo();
                utils.asyncRequest("POST", "Users", "Save", user, true, "Saving Operator", function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        clearForm();
                        utils.redirect("Users", "Index");
                    } else if (response.Status == utils.responseStatus.business) {
                        clearForm();
                        txtAccount.focus();
                        swal("Error.", response.Message, "error");
                    }
                });
            }
        }

        init();

    }();
});
