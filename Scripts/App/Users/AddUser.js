﻿/*
* @Author - José Humberto Ibarra Riestra
* 20180518 - RESQUER - Replace use of Validate.min.js for validForm function to correct compatibility problems
*/
$(function () {

    window.ROOT.AddUser = function () {

        var utils = ROOT.Utils,
            adminMenu = $("#adminMenu"),
            btnSave = $("#btnSave"),
            btnCancel = $("#btnCancel"),
            txtFirstName = $("#txtFirstName"),
            txtLastName = $("#txtLastName"),
            txtAccount = $("#txtAccount"),
            txtPassword = $("#txtPassword"),
            chkAvailable = $("#chkAvailable"),
            frmUser = $("#frmUser"),
            btnClear = $("#btnClear"),
            txtSite = $("#txtSite"),
            chkAdmin = $("#chkAdmin")
        id = 0

        function init() {
            adminMenu.addClass("active");
            initValidations();
            initEvents();
        }

        function initValidations() {
            /* frmUser.validate({
                 rules: {
                     txtFirstName: { required: true },
                     txtLastName: { required: true },
                     txtAccount: { required: true },
                     txtPassword: { required: true }
                 }
             });*/
        }

        function initEvents() {
            btnSave.click(function (e) { save(); });
            btnCancel.click(function (e) { utils.redirect("Users", "Index"); });
            btnClear.click(function (e) { clearForm(); });
        }

        function getFormInfo() {
            return {
                op_id: id,
                op_first_name: txtFirstName.val(),
                op_last_name: txtLastName.val(),
                op_user: txtAccount.val(),
                op_password: txtPassword.val(),
                site_name: txtSite.val(),
                available: chkAvailable.prop("checked"),
                user_type: chkAdmin.prop("checked")


            };
        }

        function clearForm() {
            id = 0;
            txtFirstName.val("");
            txtLastName.val("");
            txtAccount.val("");
            txtPassword.val("");
            txtSite.val("");
            chkAvailable.prop("checked", true);
            chkAdmin.prop("checked", false);
            utils.resetForm(frmUser);
        }

        function validForm() {
            var validations = "";
            if (txtFirstName.val() == "") { validations = "First name"; }
            if (txtLastName.val() == "") { validations = validations + "\n Last name "; }
            if (txtAccount.val() == "") { validations = validations + "\n Account "; }
            if (txtPassword.val() == "") { validations = validations + "\n Password "; }
            if (txtSite.val() == "") { validations = validations + "\n Site "; }
            if (validations != "") {
                swal("Error.", "These fields are required: \n" + validations, "error");
                return false;
            }
            else {
                return true;
            }

        }


         
        function save() {
            //if (frmUser.valid()) {
            if (validForm()) {
                var user = getFormInfo();
                utils.asyncRequest("POST", "Users", "Save", user, true, "Saving Operator", function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        clearForm();
                        utils.redirect("Users", "Index");
                    } else if (response.Status == utils.responseStatus.business) {
                        clearForm();
                        txtAccount.focus();
                        swal("Error.", response.Message, "error");
                    }
                });
            }
        }

        init();

    }();
});
