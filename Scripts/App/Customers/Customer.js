﻿/*
* @Author - José Humberto Ibarra Riestra
* @Date - August - 09 - 2017
*/
$(function () {

    window.ROOT.Customer = function () {

        var utils = ROOT.Utils,
            catalogsMenu = $("#catalogsMenu"),
            btnSave = $("#btnSave"),
            btnCancel = $("#btnCancel"),
            btnClear = $("#btnClear"),
            btnClearSearch = $("#btnClearSearch"),
            btnSearch = $("#btnSearch"),
            frmCustomers = $("#frmCustomers"),
            tableTab = $('.nav-tabs a[href="#tableTab"]'),
            frmTab = $('.nav-tabs a[href="#frmTab"]'),
            txtSearchCustomer = $("#txtSearchCustomer"),
            tblCustomers = $("#tblCustomers tbody"),
            txtCustomerName = $("#txtCustomerName"),
            txtSkpId = $("#txtSkpId"),
            chkItp = $("#chkItp"),
            chkAvailable = $("#chkAvailable"),
            txtSearch = $("#txtSearch"),
            id = 0,
            tableCustomers,
            messages = {
                confirmDelete: "Are you sure you want to delete the selected customer?"
            };

        function init() {
            catalogsMenu.addClass('active');
            initEvents();
            initValidations();
            initTable();
            txtSearch.attr('disabled', true);
            txtSearch.hide();
        }

        function initEvents() {
            btnSave.click(function (e) { save(); });
            btnCancel.click(function (e) { clearForm(false); });
            btnSearch.click(function (e) { search(); });
            btnClearSearch.click(function (e) { clearSearch(); });
            utils.configureFormEnterEvent([txtSearchCustomer, txtSearch], search);
            tableTab.click(function (e) { clearForm(false); });
            btnClear.click(function (e) { clearForm(true); });
        }

         function initValidations() {
            frmCustomers.validate({
                rules: {
                    txtCustomerName: { required: true },
                    txtSkpId: { required: true }
                },
                messages: {
                    txtCustomerName: {
                        required: "Please type a customer."
                    },
                    txtSkpId: {
                        required: "Please type an skp id."
                    }
                }
            });
        }

        function initTable() {
            tableCustomers = utils.createScrollableTable({
                table: tblCustomers,
                controller: "Customers",
                action: "FindByFilters",
                updateParams: function () {
                    return {
                        customerName: txtSearchCustomer.val()
                    };
                }
            });
            tableCustomers.update();
        }

         function getFormInfo() {
            return {
                Id: id,
                CustomerName: txtCustomerName.val(),
                SkpId: txtSkpId.val(),
                ItpFlag: chkItp.prop("checked"),
                Available: chkAvailable.prop("checked")
            };
        }

        function setFormInfo(customer) {
            id = customer.Id;
            txtCustomerName.val(customer.CustomerName);
            txtSkpId.val(customer.SkpId);
            chkItp.prop("checked", customer.ItpFlag);
            chkAvailable.prop("checked", customer.Available);
        }

        function save() {
            if (frmCustomers.valid()) {
                var customer = getFormInfo();
                utils.asyncRequest("POST", "Customers", "Save", customer, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        utils.createModalInfo(response.Message);
                        clearForm();
                        search();
                    } else if (response.Status == utils.responseStatus.business) {
                        utils.createModalInfo(response.Message);
                    }
                });
            }      
        }  
        
        function remove(id) {
            utils.createModalConfirm(messages.confirmDelete, function () {
                utils.asyncRequest("POST", "Customers", "Delete", { id: id }, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        utils.createModalInfo(response.Message);
                        search();
                    } else if (response.Status == utils.responseStatus.business) {
                        utils.createModalInfo(response.Message);
                    }
                });
            });            
        } 
        
         function edit(id) {            
            if (id) {
                utils.asyncRequest("GET", "Customers", "FindById", { id: id }, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        setFormInfo(response.Data);
                        frmTab.tab('show');
                        search();
                    } else if (response.Status == utils.responseStatus.business) {
                        utils.createModalInfo(response.Message);
                    }
                });
            }
        } 
        
         function search() {
            tableCustomers.clearAndUpdate();
        }            

        function clearForm(isClearButton) {
            if (isClearButton) {
                id = 0;
                txtCustomerName.val("");
                txtSkpId.val("");
                chkItp.prop("checked", false);
                chkAvailable.prop("checked", true);
                utils.resetForm(frmCustomers);
            }
            else {
                id = 0;
                txtCustomerName.val("");
                txtSkpId.val("");
                chkItp.prop("checked", false);
                chkAvailable.prop("checked", true);
                utils.resetForm(frmCustomers);
                tableTab.tab('show');
            }
        }

        function clearSearch() {
            txtSearchCustomer.val("");
            tableCustomers.clearAndUpdate();
        }

        init();

          return {
            remove: remove,
            edit: edit
        };

    } ();
});
