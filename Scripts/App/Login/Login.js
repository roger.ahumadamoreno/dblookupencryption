﻿/*
* @Author - José Humberto Ibarra Riestra
*/
$(function () {
    /*
    * LOGIN OBJECT
    */
    window.ROOT.Login = function () {

        var utils = ROOT.Utils,
            btnLogin = $("#btnLogin"),
            txtEmail = $("#UserName"),
            txtPassword = $("#password"),
            loginForm = $("#loginForm")

        function init() {
            initValidations();
            initEvents();
        }

        function initValidations() {
            loginForm.validate({
                rules: {
                    txtEmail: { required: true },
                    txtPassword: { required: true }
                }
            });
        }

        function initEvents() {
            btnLogin.click(function (e) { login(); });
            $(".form-control").keyup(function (event) {
                if (event.which === 13) {
                    login();
                }
            });
        }

        function getFormInfo() {
            return {
                UserName: txtEmail.val(),
                Password: txtPassword.val()
            };
        }

        function login() {
            swal("Can't save", "Please add at least one image to reprocess", "error");
            if (loginForm.valid()) {
                var login = getFormInfo();
                utils.asyncLoginRequest("POST", "Login", "Login", login, function (response) {
                    if (response.Status == utils.responseStatus.ok) {
                        utils.createLoginCorrectModal(response.Message, function () {
                         utils.redirect("Home", "Index")
                            //utils.redirect("Results", "Results")
                        });
                    } else if (response.Status == utils.responseStatus.business) {
                        utils.createModalInfo(response.Message);
                    }
                });
            }
        }

        // Disabled, to use Regular version and not the AJAX one.
        //init();

        return {};

    }();
});
