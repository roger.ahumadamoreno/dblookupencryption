﻿/*
* @Author - José Humberto Ibarra Riestra
* Custom Functions * 
*/
$(function () {

    /*ROOT OBJECT*/
    if (!window.ROOT) {
        window.ROOT = {};
    }

    window.ROOT.Utils = function () {
        var HOST_PATH = "/DBLookUp/",
            responseStatus = {
                ok: 1,
                business: 2,
                sessionTimeOut: 3
            }
        

        function asyncRequest(type, controller, action, params, showLoading, headerMessage, okCallback, errorCallback, completeCallback) {
            type = (!type) ? "GET" : type;
            if (showLoading == true) {
                swal({
                    title: headerMessage,
                    text: "Please wait...",
                    imageUrl: "/DBLookUp/Content/Img/DoubleRing.gif",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            }
            $.ajax({
                cache: false,
                type: type,
                url: HOST_PATH + controller + "/" + action,
                data: (type === "POST") ? JSON.stringify(params) : params,
                dataType: "json",
                contentType: "application/json;charset=utf-8"
            }).success(function (data, status, jqXHR) {
                if (showLoading == true) {
                    $(".sweet-overlay").remove();
                    $(".sweet-alert").remove();
                }
                if (okCallback) {
                    okCallback(data);
                }

            }).error(function (jqXHR, status, errorThrown) {
                if (showLoading == true) {
                    $(".sweet-overlay").remove();
                    $(".sweet-alert").remove();
                }
                if (errorCallback) {
                    errorCallback();
                }
            }).complete(function (jqXHR, status) {
                if (completeCallback) {
                    completeCallback();
                }
            });
        }

        function syncRequest(type, controller, action, params, okCallback, errorCallback, completeCallback) {
            type = (!type) ? "GET" : type;
            $.ajax({
                cache: false,
                async: false,
                type: type,
                url: HOST_PATH + controller + "/" + action,
                data: (type === "POST") ? JSON.stringify(params) : params,
                dataType: "json",
                contentType: "application/json;charset=utf-8"
            }).success(function (data, status, jqXHR) {
                if (okCallback) {
                    okCallback(data);
                }
            }).error(function (jqXHR, status, errorThrown) {
                if (errorCallback) {
                    errorCallback();
                }
            }).complete(function (jqXHR, status) {
                if (completeCallback) {
                    completeCallback();
                }
            });
        }

        function resetForm(form) {
            $("label.error").hide();
            $(".error").removeClass("error");
        }

        function configureFormEnterEvent(textFields, enterFunction) {
            var txt;
            for (var i = 0; i < textFields.length; i++) {
                txt = textFields[i];
                if (txt) {
                    txt.keyup(function (e) {
                        if (e.which !== 13) {
                            return;
                        }
                        e.preventDefault();
                        enterFunction();
                    });
                }
            }
        }

        function configureTextBoxEnterEvent(txt, enterFunction) {
            if (txt) {
                txt.keyup(function (e) {
                    if (e.which !== 13) {
                        return;
                    }
                    e.preventDefault();
                    enterFunction();
                });
            }
        }

        function createScrollableTable(config) {
            var scrollableTable = function () {

                var table,
                    tableSorter,
                    controller,
                    action,
                    pageNum = 1,
                    pageSize = 50,
                    format = 1,
                    updateParams;

                function init(config) {
                    table = config.table;
                    tableSorter = config.tableSorter;
                    controller = config.controller;
                    action = config.action;
                    updateParams = config.updateParams;
                    // initEvents();
                }

                /*function initEvents() {
                     var win = $(window);
                     win.scroll(function () {
                         if ($(document).height() - win.height() == win.scrollTop()) {
                             update();
                         }
                     });
                 }*/

                function update() {
                    var params = {
                        pageNum: pageNum,
                        pageSize: pageSize,
                        format: format
                    };

                    if (updateParams && $.isFunction(updateParams)) {
                        $.extend(params, updateParams());
                    }

                    $.ajax({
                        cache: false,
                        type: "GET",
                        url: HOST_PATH + controller + "/" + action,
                        data: params,
                        dataType: "json",
                        contentType: "application/json;charset=utf-8",
                        success: function (response) {
                            pageNum++;
                            table.append(response.Data.Rows);
                            table.trigger("update");
                            var rowCount = table.find("tr").length;
                            if (rowCount == 500) {
                                swal("Warning", "The limit of rows per search is 500, to reduce the search result use more specific filters.", "warning");
                            }
                        }
                    });
                }

                function clearAndUpdate() {
                    table.find("tr").remove();
                    pageNum = 1;
                    update();
                }

                init(config);

                return {
                    update: update,
                    clearAndUpdate: clearAndUpdate
                };

            }(config);

            return scrollableTable;
        }

        function getHtmlTableRowsSelected(table) {
            var idsSelected = [];
            table.find('input[type="checkbox"]:checked').each(function () {
                var checkBox = $(this);
                idsSelected.push(checkBox.val());
            });
            return idsSelected;
        }

        function getTableCheckBoxChecked(table) {
            var idsSelected = 0;
            table.find('input[type="checkbox"]:checked').each(function () {
                var checkBox = $(this);
                idsSelected++;
            });
            return idsSelected;
        }

        function removeHtmlRowsSelected(table) {
            var idsSelected = [];
            table.find('input[type="checkbox"]:checked').each(function () {
                var checkBox = $(this);
                $(this).closest("tr").remove();
            });
        }

        function setHtmlTableRowsSelected(table, elements) {
            if (elements.length > 0) {
                table.find('input[type="checkbox"]').each(function () {
                    var checkBox = $(this);
                    for (var i = 0; i < elements.length; i++) {
                        if (checkBox.val() == elements[i]) {
                            checkBox.prop("checked", true);
                            return;
                        }
                    }
                });
            }
        }

        function clearHtmlTableRowsSelected(table) {
            table.find('input[type="checkbox"]:checked').each(function () {
                var checkBox = $(this);
                checkBox.prop("checked", false);
            });
        }

        function generateSelectList(combo, controller, action, params, showDefaultOption, setValue) {
            $.ajax({
                type: "GET",
                url: HOST_PATH + controller + "/" + action,
                data: params,
                success: function (elements) {
                    if (showDefaultOption) {
                        combo.append($('<option>', {
                            value: "",
                            text: "-Please Select-"
                        }));
                    }
                    for (var i = 0; i < elements.length; i++) {
                        var element = elements[i];
                        combo.append($('<option>', {
                            value: element.Value,
                            text: element.Label
                        }));
                    }
                    if (setValue) {
                        combo.val(setValue);
                    }
                },
            });
        }

        function convertToComboList(combo, elements, showDefaultOption) {
            if (showDefaultOption) {
                combo.append($('<option>', {
                    value: "",
                    text: "-Please Select-"
                }));
            }
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                combo.append($('<option>', {
                    value: element.Value,
                    text: element.Label
                }));
            }
        }

        function validateChekckBoxChecked(checkBox) {
            if (checkBox.is(":checked")) {
                return true
            } else {
                return false;
            }
        }

        function redirect(controller, action, params) {
            var queryString = "?";
            for (var prop in params) {
                queryString += prop + "=" + params[prop] + "&";
            }
            window.location = HOST_PATH + controller + "/" + action + queryString;
        }

        function generateReport(controller, action, params) {
            var queryString = "?";
            for (var prop in params) {
                queryString += prop + "=" + params[prop] + "&";
            }
            window.open(HOST_PATH + controller + "/" + action + queryString, "_blank");
        }

        function checkIfSessionExist() {
            asyncRequest("Post", "Session", "ExistActiveSession", {}, false, null, function (response) {
                if (response.Status == responseStatus.ok) {
                    var isActive = response.Data;
                    if (!isActive) {
                        createLoginModal();
                    }
                } else if (response.Status == responseStatus.business) {
                    createModalInfo("Error");
                }
            });
            // setTimeout(checkIfSessionExist, 3636000);
            //setTimeout(checkIfSessionExist, 132000);
            setTimeout(checkIfSessionExist, 10908000);
        }

        function createLoginModal() {
            var modal = "";
            modal += '<div class="modal fade" id="modal" tabindex="-1" role="dialog"  data-backdrop="static"  aria-labelledby="myModalLabel" aria-hidden="true">';
            modal += '<div class="modal-dialog"><div class="modal-content"><div class="modal-header">';
            modal += '<h4 class="modal-title" id="myModalLabel"><strong>Your Session Has Expired, Please Enter Your Credentials..</strong></h4></div>';
            modal += '<div class="modal-body">';
            modal += '<form class="form-horizontal m-t-40" id="loginForm">';
            modal += '<div class="form-group ">';
            modal += '<div class="col-xs-12">';
            modal += '<span class="input-group-addon"><i class="fa fa-user"></i></span>';
            modal += '<input class="form-control" id="email" type="text" placeholder="Email" name="email" maxlength="30">';
            modal += '</div>';
            modal += '</div>';
            modal += '<div class="form-group ">';
            modal += '<div class="col-xs-12">';
            modal += '<span class="input-group-addon"><i class="fa fa-key"></i></span>';
            modal += '<input class="form-control" type="password" id="password" placeholder="Password" name="password" maxlength="30">';
            modal += '</div>';
            modal += '</div>';
            modal += '<div class="form-group">';
            modal += '<div class="col-xs-12">';
            modal += '<button id="login" type="button" class="btn btn-lg btn-info btn-block">';
            modal += 'Login';
            modal += '</button>';
            modal += '</div>';
            modal += '</div>';
            modal += '</form></div></div></div>';
            dialogo = $(modal);
            var button = dialogo.find("#login");
            var txtEmail = dialogo.find("#email");
            var txtPassword = dialogo.find("#password");
            txtEmail.focus();
            initModalFormEvents(button, txtEmail, txtPassword, dialogo);
            dialogo.modal("show");
        }

        function initModalFormEvents(button, txtEmail, txtPassword, popup) {
            popup.on("shown", function () {
                txtEmail.focus();
            });
            button.click(function (e) {
                var email = txtEmail.val();
                var password = txtPassword.val();
                if (!email) {
                    createModalInfo("User name is required.");
                } else if (!password) {
                    createModalInfo("Password is required.");
                } else {
                    asyncRequest("POST", "Login", "Login", { account: email, password: password }, false, null, function (response) {
                        if (response.Status == responseStatus.ok) {
                            popup.modal("hide");
                            $("#sessionStatus").load(location.href + " #sessionStatus");
                        } else if (response.Status == responseStatus.business) {
                            createModalInfo(response.Message);
                        }
                    });
                }
            });
        }

        function getEditTemplate() {
            var template = "";
            template += '<div class="modal fade-scale-custom" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
            template += '<div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close"data-dismiss="modal">';
            template += '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel"><strong>Edit Package</strong></h4></div>';
            template += '<div class="modal-body">';
            template += '<form class="form-horizontal" enctype="multipart/form-data">';
            template += '<div class="form-group">';
            template += '<label  for="txtTrackingNumber">Tracking Number</label>';
            template += '<input type="text" class="form-control" id="eTxtTrackingNumber" placeholder="Tracking Number" maxlength="100">'
            template += '</div>';
            template += '<div class="form-group">';
            template += '<label for="cboPackageType">Package Type</label>';
            template += '<select class="form-control select2" id="eCboPackageType" name="cboPackageType" style="margin-top:-3px; width: 100%; height:100%;"><option></option><option value="Closing">Closing</option><option value="Trailing">Trailing</option></select>';
            template += '</div>';
            template += '<div class="form-group">';
            template += '<label class="cr-styled">';
            template += '<input type="checkbox" id="eChkUrgent" name="chkUrgent">';
            template += '<i class="fa"></i>Urgent Flag</label>';
            template += '</div>';
            template += '<div class="form-group">'
            template += '<label for="dpkDateFrom" class="control-label">Received Date</label>';
            template += '<input type="text" class="form-control" placeholder="Received Date Time" id="eDpkReceived">';
            template += '<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>';
            template += '</div>';
            template += '<div class="form-group">';
            template += '<label  for="txtComments">Comments</label>';
            template += '<input type="text" class="form-control" id="eTxtComments" placeholder="Comments" maxlength="500">';
            template += '</div>';
            template += '</form>';
            template += '<div class="modal-footer">';
            template += '<button type="button" class="btn btn-default" data-dismiss="modal"><i class="ion-android-close"></i>Close</button>';
            template += '<button type="button" class="btn btn-info"><i class="ion-android-checkmark"></i>Save Changes</button>';
            template += '</div></div></div></div></div>';
            return template;
        }

        function getConfirmTemplate(tittle, label) {
            var template = "";
            template += '<div class="modal fade-scale-custom" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
            template += '<div class="modal-dialog modal-md"><div class="modal-content"><div class="modal-header"><button type="button" class="close"data-dismiss="modal">';
            template += '<span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel"><strong>' + tittle + '</strong></h4></div>';
            template += '<div class="modal-body">';
            template += '<form role="form" class="p-20">';
            template += '<div class="form-group">';
            template += '<label for="txtImageNumber">' + label + '</label>';
            template += '<input type="text" class="form-control" id="txtImageNumber" placeholder="' + label + '" maxlength="10" autofocus>';
            template += '</div>';
            template += '<div class="form-group">';
            template += '<label for="cboDinProcess" id="lblNext">Please Select The Next Process</label>';
            template += '<select class="select2 dynamic" id="cboDinProcess" name="cboDinProcess" style="margin-top:-3px;"><option value="" disabled selected>- Please Select -</option></select>';
            template += '</div></form>';
            template += '<div class="modal-footer">';
            template += '<button type="button" class="btn btn-default" data-dismiss="modal"><i class="ion-android-close"></i>Close</button>';
            template += '<button type="button" class="btn btn-info"><i class="ion-android-checkmark"></i>Save</button>';
            template += '</div></div></div></div></div>';
            return template;
        }

        function addHtmlTableRows(table, elements, columns, checkBoxSelection, headers, removeButton) {
            if (headers !== null) {
                table.append('<tr></tr>');
                for (var h = 0; h < headers.length; h++) {
                    var header = headers[h];
                    table.find('tr').append('<th>' + header + '</th>')
                }
            }
            for (var i = 0; i < elements.length; i++) {
                var element = elements[i];
                var tr = $('<tr>');
                tr.addClass('notChecked');
                if (checkBoxSelection) {
                    tr.append($('<td>').html('<label class="cr-styled"><input type="checkbox" value="' + element.Id + '" class="" /><i class="fa"></i></label>'));
                }
                for (var j = 0; j < columns.length; j++) {
                    tr.append($('<td>').html(element[columns[j]]))
                }
                table.find("tbody").append(tr);
                if (removeButton) {
                    var btn = '<td><button class="btn btn-primary" type="button" name="btnRemoveRow" id="btnRemoveRow"><span class="glyphicon glyphicon-remove"></span></button></td>';
                    jqueryButton = $(btn);
                    jqueryButton.click(function (e) {
                        $(this).closest('tr').remove();
                    });
                    tr.append(jqueryButton);
                }
            }
        }

        /* Public functions and properties */
        return {
            asyncRequest: asyncRequest,
            syncRequest: syncRequest,
            resetForm: resetForm,
            responseStatus: responseStatus,
            createScrollableTable: createScrollableTable,
            configureFormEnterEvent: configureFormEnterEvent,
            getHtmlTableRowsSelected: getHtmlTableRowsSelected,
            setHtmlTableRowsSelected: setHtmlTableRowsSelected,
            clearHtmlTableRowsSelected: clearHtmlTableRowsSelected,
            validateChekckBoxChecked: validateChekckBoxChecked,
            redirect: redirect,
            generateSelectList: generateSelectList,
            configureTextBoxEnterEvent: configureTextBoxEnterEvent,
            generateReport: generateReport,
            createLoginModal: createLoginModal,
            checkIfSessionExist: checkIfSessionExist,
            HOST_PATH: HOST_PATH,
            convertToComboList: convertToComboList,
            getEditTemplate: getEditTemplate,
            getConfirmTemplate: getConfirmTemplate,
            addHtmlTableRows: addHtmlTableRows,
            removeHtmlRowsSelected: removeHtmlRowsSelected,
            getTableCheckBoxChecked: getTableCheckBoxChecked
        };

    }();
});